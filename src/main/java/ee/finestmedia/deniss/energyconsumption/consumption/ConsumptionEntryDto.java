package ee.finestmedia.deniss.energyconsumption.consumption;


import java.time.LocalDate;

public class ConsumptionEntryDto {

    private LocalDate time;
    private double value;

    public ConsumptionEntryDto(LocalDate time, Double value) {
        this.time = time;
        this.value = value;
    }

    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
