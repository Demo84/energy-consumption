package ee.finestmedia.deniss.energyconsumption.consumption;

public enum GroupingType {
    day, week, month
}
