package ee.finestmedia.deniss.energyconsumption.consumption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ConsumptionService {

    @Autowired
    private ConsumptionRepository consumptionRepository;

    public SortedMap<LocalDate, Double> get(Search search) {
        SortedMap<LocalDate, Double> entries = consumptionRepository.find(search.from, search.to);
        switch (search.groupingType) {
            case day:
                return entries;
            case week: {
                TreeMap<LocalDate, Double> weekEntries = entries.entrySet().stream()
                        .collect(Collectors.groupingBy(time -> time.getKey().with(TemporalAdjusters.previousOrSame(DayOfWeek.of(1))), TreeMap::new,
                                Collectors.summingDouble(Map.Entry::getValue)));
                Double value = weekEntries.remove(search.from.with(TemporalAdjusters.previousOrSame(DayOfWeek.of(1))));
                weekEntries.put(search.from, value);
                return weekEntries;
            }
            case month: {
                TreeMap<LocalDate, Double> monthEntries = entries.entrySet().stream()
                        .collect(Collectors.groupingBy(time -> time.getKey().with(TemporalAdjusters.firstDayOfMonth()), TreeMap::new,
                                Collectors.summingDouble(Map.Entry::getValue)));
                Double value = monthEntries.remove(search.from.with(TemporalAdjusters.firstDayOfMonth()));
                monthEntries.put(search.from, value);
                return monthEntries;
            }
            default:
                return Collections.emptySortedMap();
        }
    }


}
