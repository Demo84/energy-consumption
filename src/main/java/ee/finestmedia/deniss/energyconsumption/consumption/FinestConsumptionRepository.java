package ee.finestmedia.deniss.energyconsumption.consumption;

import org.springframework.stereotype.Repository;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class FinestConsumptionRepository implements ConsumptionRepository {

    private SortedMap<LocalDate, Double> entriesMap = new ConcurrentSkipListMap<>();

    private static final String KWH_URL = "https://finestmedia.ee/kwh/?start=%s&end=%s";
    private static final String HOUR_CONSUMPTION_XML_TAG = "HourConsumption";
    private static final String TIME_XML_ARG = "ts";
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ISO_DATE_TIME;

    private static final Logger LOGGER = Logger.getLogger(FinestConsumptionRepository.class.getClass().getName());

    public SortedMap<LocalDate, Double> find(LocalDate from, LocalDate to) {
        boolean noMissingDates = streamPeriod(from, to)
                .allMatch(localDate -> entriesMap.containsKey(localDate));

        if (!noMissingDates) {
            try {
                String url = String.format(KWH_URL, from.toString(), to.toString());
                entriesMap.putAll(loadAndProcessResponse(url));
            } catch (SAXException | IOException e) {
                LOGGER.severe("There is an error when loading/parsing xml from Finestmedia website");
                throw new RuntimeException(e);
            }
        }

        return streamPeriod(from, to)
                .collect(Collectors.toMap(Function.identity(), date -> entriesMap.get(date), (o, n)-> n, TreeMap::new));
    }

    private SortedMap<LocalDate, Double> loadAndProcessResponse(String url) throws SAXException, IOException {
        XMLReader myReader = XMLReaderFactory.createXMLReader();
        SortedMap<LocalDateTime, Double> entries = new TreeMap<>();
        myReader.setContentHandler(new DefaultHandler(){
            private LocalDateTime activeTimeEntry;

            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) {
                if (qName.equalsIgnoreCase(HOUR_CONSUMPTION_XML_TAG)) {
                    activeTimeEntry = LocalDateTime.parse(attributes.getValue(TIME_XML_ARG), TIME_FORMATTER)
                            .truncatedTo(ChronoUnit.HOURS);
                }
            }

            @Override
            public void characters(char[] ch, int start, int length) {
                if (activeTimeEntry != null) {
                    entries.put(activeTimeEntry, Double.valueOf(new String(ch, start, length)));
                    activeTimeEntry = null;
                }
            }
        });

        LOGGER.info(String.format("Loading %s url", url));
        myReader.parse(new InputSource(new URL(url).openStream()));

        removeHoursAtBeginning(entries);
        removeHoursAtEnd(entries);

        return groupByDays(entries);

    }
    private static SortedMap<LocalDate, Double> groupByDays(Map<LocalDateTime, Double> entries) {
       return entries.entrySet().stream()
                .collect(Collectors.groupingBy(time -> time.getKey().toLocalDate(), TreeMap::new,
                        Collectors.summingDouble(Map.Entry::getValue)));
    }

    static void removeHoursAtEnd(SortedMap<LocalDateTime, Double> entries) {
        LocalDateTime lastTime = entries.lastKey();
        int hour = lastTime.getHour();
        while (hour != 23) {
            entries.remove(lastTime);
            lastTime = lastTime.minusHours(1);
            hour = lastTime.getHour();
        }
    }

    static void removeHoursAtBeginning(SortedMap<LocalDateTime, Double> entries) {
        LocalDateTime firstTime = entries.firstKey();
        int hour = firstTime.getHour();
        while (hour != 0) {
            entries.remove(firstTime);
            firstTime = firstTime.plusHours(1);
            hour = firstTime.getHour();
        }
    }

    private static Stream<LocalDate> streamPeriod(LocalDate from, LocalDate to) {
        return Stream.iterate(from, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(from, to) + 1);
    }
}
