package ee.finestmedia.deniss.energyconsumption.consumption;

import java.time.LocalDate;

public class Search {
    public Search(LocalDate from, LocalDate to, GroupingType groupingType) {
        this.groupingType = groupingType;
        this.from = from;
        this.to = to;
    }

    GroupingType groupingType;
    LocalDate from;
    LocalDate to;
}
