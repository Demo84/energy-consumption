package ee.finestmedia.deniss.energyconsumption.consumption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ConsumptionController {

    @Autowired
    ConsumptionService consumptionService;

    @CrossOrigin
    @RequestMapping("get")
    public List<ConsumptionEntryDto> get(@RequestParam String from, @RequestParam String to, @RequestParam GroupingType grouping) throws IOException, SAXException {
        LocalDate fromDate = LocalDate.parse(from);
        LocalDate toDate = LocalDate.parse(to);

        if (toDate.plusDays(1).isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("The To date could not be in a future");
        }

        if (fromDate.isBefore(LocalDate.now().minusYears(2))) {
            throw new IllegalArgumentException("The From date could not be older than 2 years");
        }

        Search search = new Search(fromDate, toDate, grouping);
        return consumptionService.get(search).entrySet().stream()
                .map(entry -> new ConsumptionEntryDto(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

}
