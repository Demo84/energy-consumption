import { Component, OnInit } from '@angular/core';

import { EntriesService } from '../entries.service';
import { Entry } from '../entries/entry';
import { Search } from '../entries-search-form/search';
import { composeDate } from '../utilities';
import { CalculatePricePipe } from '../calculate-price.pipe';

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.css']
})
export class EntriesComponent implements OnInit {
  entries: Entry[];

  constructor(private entriesService: EntriesService) { }

  ngOnInit() {
    var from = new Date();
    from.setMonth(from.getMonth() - 1);
    var to = new Date();
    to.setDate(to.getDate() - 1);
    this.entriesService.entriesChanged
      .subscribe(
        (entries: Entry[]) => {
          this.entries = entries;
        }
      );
    this.entriesService.loadEntries(new Search(composeDate(from), composeDate(to), "day"));
  }

}
