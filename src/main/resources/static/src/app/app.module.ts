import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { EntriesComponent } from './entries/entries.component';
import { EntriesSearchFormComponent } from './entries-search-form/entries-search-form.component';
import { EntriesCalculateFormComponent } from './entries-calculate-form/entries-calculate-form.component';
import { CalculatePricePipe } from './calculate-price.pipe';
import { PeriodPipe } from './period.pipe';

@NgModule({
  declarations: [
    AppComponent,
    EntriesComponent,
    EntriesSearchFormComponent,
    EntriesCalculateFormComponent,
    CalculatePricePipe,
    PeriodPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
