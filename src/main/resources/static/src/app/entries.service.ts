import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLSearchParams } from '@angular/http'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Entry } from './entries/entry';
import { Search } from './entries-search-form/search';

@Injectable({
  providedIn: 'root'
})
export class EntriesService {

  static readonly defaultPrice = 30;
  static readonly defaultPeriodType = "day";

  price = EntriesService.defaultPrice;
  periodType = EntriesService.defaultPeriodType;

  entriesChanged = new EventEmitter<Entry[]>();
  priceChanged = new EventEmitter<number>();
  periodTypeChanged = new EventEmitter<string>();
  errorOccurred = new EventEmitter<string[]>();

  constructor(private http: HttpClient) {}

  changePrice(price: number) {
    this.price = price;
    this.priceChanged.emit(price);
  }

  changePeriodType(periodType: string) {
    this.periodType = periodType;
    this.periodTypeChanged.emit(periodType);
  }

  loadEntries(search: Search) {
    let params = new URLSearchParams();
    params.set('from', search.from);
    params.set('to', search.to);
    params.set('grouping', search.grouping);
    if (search.grouping != this.periodType){
      this.changePeriodType(search.grouping);
    }

    this.errorOccurred.emit(null);
    this.http.get<Entry[]>("http://localhost:8080/get?" + params.toString()).pipe(
      catchError(this.handleError([]))
    ).subscribe(entries => {
      this.entriesChanged.emit(entries);
    });
  }

  private handleError<T> (result?: T) {
    return (error: any): Observable<T> => {
      this.errorOccurred.emit(error.error.message);
      return of(result as T);
    };
  }

}
