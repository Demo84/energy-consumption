import { Pipe, PipeTransform } from '@angular/core';
import { EntriesService } from './entries.service';

@Pipe({name: 'calculatePrice', pure: false})
export class CalculatePricePipe implements PipeTransform {

  price: number;

  constructor(private entriesService: EntriesService) {
    this.entriesService.priceChanged
      .subscribe(
        (price: number) => {
          this.price = price;
        }
      );
    this.price = this.entriesService.price;
  }

  transform(value: number): number {
    return this.price * value;
  }
}
