import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntriesCalculateFormComponent } from './entries-calculate-form.component';

describe('EntriesCalculateFormComponent', () => {
  let component: EntriesCalculateFormComponent;
  let fixture: ComponentFixture<EntriesCalculateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntriesCalculateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntriesCalculateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
