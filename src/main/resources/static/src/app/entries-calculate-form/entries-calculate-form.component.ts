import { Component, OnInit } from '@angular/core';
import { EntriesService } from '../entries.service';

@Component({
  selector: 'app-entries-calculate-form',
  templateUrl: './entries-calculate-form.component.html',
  styleUrls: ['./entries-calculate-form.component.css']
})
export class EntriesCalculateFormComponent implements OnInit {

  price: number

  constructor(private entriesService: EntriesService) { }

  ngOnInit() {
    this.price = this.entriesService.price;
  }

  onSubmit(form) {
    this.entriesService.changePrice(form.value.price);
  }

}
