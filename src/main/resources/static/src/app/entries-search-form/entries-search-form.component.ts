import { Component, OnInit } from '@angular/core';
import { EntriesService } from '../entries.service';
import { Search } from './search';
import { composeDate } from '../utilities';

@Component({
  selector: 'app-entries-search-form',
  templateUrl: './entries-search-form.component.html',
  styleUrls: ['./entries-search-form.component.css']
})
export class EntriesSearchFormComponent {

  groupings = [{'key': 'day', 'name': 'by Day'},
                {'key': 'week', 'name': 'by Week'},
                {'key': 'month', 'name': 'by Month'}];

  model: Search;
  minDate: string;
  maxDate: string;
  error: string;

  constructor(private entriesService: EntriesService) {
    let toDate = new Date();
    toDate.setDate(toDate.getDate() - 1);

    let fromDate = new Date();
    fromDate.setMonth(fromDate.getMonth() - 1);

    let minPossibleDate = new Date();
    minPossibleDate.setFullYear(fromDate.getFullYear() - 2);

    this.model = new Search(composeDate(fromDate), composeDate(toDate), this.groupings[0].key);
    this.minDate = composeDate(minPossibleDate);
    this.maxDate = composeDate(toDate);

    this.entriesService.errorOccurred
      .subscribe(
        (error: string) => {
          this.error = error;
        }
      );
  }

  onSubmit(form) {
    this.entriesService.loadEntries(form.value);
  }

}
