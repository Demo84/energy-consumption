import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntriesSearchFormComponent } from './entries-search-form.component';

describe('EntriesSearchFormComponent', () => {
  let component: EntriesSearchFormComponent;
  let fixture: ComponentFixture<EntriesSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntriesSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntriesSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
