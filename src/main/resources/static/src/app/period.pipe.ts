import { Pipe, PipeTransform } from '@angular/core';
import { EntriesService } from './entries.service';
import { composeDate } from './utilities';

@Pipe({name: 'period', pure: false})
export class PeriodPipe implements PipeTransform {

  type: string;

  constructor(private entriesService: EntriesService) {
    this.entriesService.periodTypeChanged
      .subscribe(
        (type: string) => {
          this.type = type;
        }
      );
    this.type = this.entriesService.periodType;
  }

  transform(dateString: string): string {
    let date = new Date(dateString);

    switch(this.type){
      case "day": {
        return dateString;
      }
      case "week": {
        let toDate = new Date(date);
        toDate.setDate(toDate.getDate() + Math.abs((date.getDay() || 7) - 7));
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        if (toDate > yesterday) {
          toDate = yesterday;
        }

        return composeDate(date) + ' - ' + composeDate(toDate);
      }
      case "month": {
        let toDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        if (toDate > yesterday) {
          toDate = yesterday;
        }
        return composeDate(date) + ' - ' + composeDate(toDate);
      }
    }
    return this.type;
  }
}
