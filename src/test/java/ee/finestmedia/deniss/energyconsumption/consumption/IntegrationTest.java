package ee.finestmedia.deniss.energyconsumption.consumption;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.SortedMap;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IntegrationTest {

    @Autowired
    private ConsumptionRepository repository;

    @Test
    public void processResponse() {
        SortedMap<LocalDate, Double> result = repository.find(LocalDate.now().minusDays(5), LocalDate.now().minusDays(1));

        Assert.assertTrue(result.containsKey(LocalDate.now().minusDays(1)));
        Assert.assertTrue(!result.get(LocalDate.now().minusDays(1)).isNaN());
        Assert.assertTrue(!result.get(LocalDate.now().minusDays(1)).isInfinite());
        Assert.assertTrue(result.containsKey(LocalDate.now().minusDays(2)));
        Assert.assertTrue(result.containsKey(LocalDate.now().minusDays(3)));
        Assert.assertTrue(result.containsKey(LocalDate.now().minusDays(4)));
        Assert.assertTrue(result.containsKey(LocalDate.now().minusDays(5)));
    }
}