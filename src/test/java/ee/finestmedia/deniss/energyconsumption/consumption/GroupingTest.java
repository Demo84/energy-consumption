package ee.finestmedia.deniss.energyconsumption.consumption;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.SortedMap;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GroupingTest {

    @MockBean
    ConsumptionRepository mockRepository;

    @Autowired
    public ConsumptionService consumptionService;

    @Test
    public void groupByWeek(){
        when(mockRepository.find(LocalDate.now().minusDays(65), LocalDate.now().minusDays(1)))
                .thenReturn(MockEntries.MOCK_ENTRIES);
        Search search = new Search(LocalDate.now().minusDays(65), LocalDate.now().minusDays(1), GroupingType.week);
        SortedMap<LocalDate, Double> weekResults = consumptionService.get(search);

        Assert.assertEquals(10, weekResults.size());
        Iterator<LocalDate> iterator = weekResults.keySet().iterator();
        LocalDate week1 = iterator.next();
        LocalDate week2 = iterator.next();
        Assert.assertEquals(week1.plusWeeks(1), week2);
        LocalDate week3 = iterator.next();
        Assert.assertEquals(week2.plusWeeks(1), week3);
    }

    @Test
    public void groupByMonth(){
        when(mockRepository.find(LocalDate.now().minusDays(65), LocalDate.now().minusDays(1)))
                .thenReturn(MockEntries.MOCK_ENTRIES);
        Search search = new Search(LocalDate.now().minusDays(65), LocalDate.now().minusDays(1), GroupingType.month);
        SortedMap<LocalDate, Double> monthResults = consumptionService.get(search);
        Assert.assertEquals(3, monthResults.size());

        Iterator<LocalDate> iterator = monthResults.keySet().iterator();
        LocalDate month1 = iterator.next();
        LocalDate month2 = iterator.next();
        Assert.assertEquals(month1.plusMonths(1).getMonth(), month2.getMonth());
        LocalDate month3 = iterator.next();
        Assert.assertEquals(month2.plusMonths(1), month3);

        Assert.assertEquals(month1.getMonth(), LocalDate.now().minusMonths(2).getMonth());
    }


}
