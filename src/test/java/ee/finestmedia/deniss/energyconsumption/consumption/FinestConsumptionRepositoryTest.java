package ee.finestmedia.deniss.energyconsumption.consumption;


import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.TreeMap;

public class FinestConsumptionRepositoryTest {

    @Test
    public void testRemoveHoursAtBeginning() {
        TreeMap<LocalDateTime, Double> timeEntries = new TreeMap<>(MockEntries.MOCK_TIMEENTRIES);
        FinestConsumptionRepository.removeHoursAtBeginning(timeEntries);
        Assert.assertEquals(27, timeEntries.size());
        Assert.assertEquals(LocalDateTime.of(2018, 8, 11, 0, 0), timeEntries.firstKey());
    }

    @Test
    public void testRemoveHoursAtEnd() {
        TreeMap<LocalDateTime, Double> timeEntries = new TreeMap<>(MockEntries.MOCK_TIMEENTRIES);
        FinestConsumptionRepository.removeHoursAtEnd(timeEntries);
        Assert.assertEquals(26, timeEntries.size());
        Assert.assertEquals(LocalDateTime.of(2018, 8, 11, 23, 0), timeEntries.lastKey());
    }


}
