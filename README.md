# Energy Consumption Test Assignment

This is source code for test assignment for FinestMedia

## Introduction

For server I've chosen Spring Boot framework. For testing I'm using JUnit and Mockito. 
On a client side I've chosen AngularJS

## Installation

First start the server. Execute commands in root directory 
```
./mvnw package
java -jar target/energy-consumption-0.0.1-SNAPSHOT.jar
```

To start client part go to static resources folder
```
cd src/main/resources/static/
```
and execute 
```
npm install
ng serve
```

The application should be available at
```
http://localhost:4200/
```


